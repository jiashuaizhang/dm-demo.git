达梦数据库(DM Database)是武汉达梦数据库股份有限公司研发的新一代大型通用关系型国产数据库，全面支持 SQL 标准和主流编程语言接口/开发框架。行列融合存储技术，在兼顾 OLAP 和 OLTP 的同时，满足 HTAP 混合应用场景。

基于spring boot，本项目集成了mytatis和dm数据库。