package com.dm.demo.controller;

import com.dm.demo.entity.db.AlarmStrategy;
import com.dm.demo.mapper.AlarmStrategyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RequestMapping("/strategy")
@RestController
public class DbController {

	@Autowired
	private AlarmStrategyMapper alarmStrategyMapper;

	@PostMapping(value = "/list")
	public List<AlarmStrategy> listAlarmStrategy() {
		return alarmStrategyMapper.selectList(null);
	}

	@PostMapping(value = "/add")
	public String addAlarmStrategy() {
		AlarmStrategy alarmStrategy = new AlarmStrategy();
		alarmStrategy.setStrategyName("测试策略1");
		alarmStrategy.setCreateTime(new Date());
		alarmStrategy.setUpdateTime(new Date());

		alarmStrategyMapper.insert(alarmStrategy);
		return "ok";
	}

}
