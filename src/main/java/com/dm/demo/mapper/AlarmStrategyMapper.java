package com.dm.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dm.demo.entity.db.AlarmStrategy;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AlarmStrategyMapper extends BaseMapper<AlarmStrategy> {

}
