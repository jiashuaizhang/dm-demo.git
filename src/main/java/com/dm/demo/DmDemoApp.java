package com.dm.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DmDemoApp {

	public static void main(String[] args) {
		SpringApplication.run(DmDemoApp.class, args);
	}

}
