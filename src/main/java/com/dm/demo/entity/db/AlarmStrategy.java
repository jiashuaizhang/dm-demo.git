package com.dm.demo.entity.db;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @author: jszhang
 * @date: 2024/4/11-10:23
 */
@TableName("THC.ALARM_STRATEGY")
@Data
public class AlarmStrategy {

    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    private String strategyName;

    private String alarmType;

    private String alarmKeyword;

    private String keywordType;

    private String alarmStartTime;

    private String alarmEndTime;

    private Date createTime;

    private Date updateTime;

}
